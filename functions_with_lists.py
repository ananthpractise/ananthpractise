"""
list1 = [1,2,3,4,5]
list2 = ["hello", "world", "new", "book"]
list3 = [1.5, 9.9, 8.8, 4.6]
"""
"""
def f1(li):
    return li[2]

print(f1(list1))
print(f1(list2))
print(f1(list3))
"""
"""
def f2(li2):
    prod = 1
    for val in li2:
        prod = prod*val
    return prod
print(f2(list1))

"""
"""
def f3(li):
    li.append(6)
    li.remove(3)
    li.insert(2,6)
    return li

print(f3(list1))
"""
"""
list1 = ["Hello", "World", "Name"]
"""
"""
def func(li):
    print(li[1])
    #print(li[0] + "frog")
    str = li.pop(0)
    strnew = str + "frog"
    print(strnew)
    #li.remove("Hello")
    print(li)


func(list1)
"""
"""
list1 = [1,2,3,4,5,6]
def f1(li):
    for x in li:
        print(x)
f1(list1)
"""
"""
list1 = (range(10))
list2 = range(4,8)
print(list2)
list3 = range(5,21,5)
print(list3)
"""
"""
list1 = [1,2,3,4,5,6]
def iterator(a):
    for x in range(0,len(a)):
        print(a[x
    """
"""
list1 = [1,2,3,4,5,6]
def modify(a):
    for x in range(0,len(a)):
        a[x]+=3
    print(a)
modify(list1)
"""
"""
list1 = [[1,2,3],[4,5,6],[7,8,9]]
def func(a):
    emptylist = []
    for val in a:
        for x in val:
             emptylist.append(x)
        print(emptylist)

    func(list1)
"""
"""
list1 = [1,2,3,4]
def f1(a):
    for x in a:
        print(x)
f1(list1)
"""
"""
list1 = range(0,3)
list2 = range(2,5)
list3 = range(2,13,5)
print(list1, list2, list3)
"""
"""
def func(a):
    empty = []
    for x in a:
        for b in x:
            empty.append(b)
    print(empty)

list1 = [[1,2],[0,1],[3,4]]
func(list1)
"""
