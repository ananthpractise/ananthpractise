"""
list1 = ["one", "two", "three", "four"]
list2 = [1,2,3,4]
list3 = ["Hello", 9, True, "World", 9]
print(list1)
print(list2.index(2))
#print(list3.index(2))
"""
"""
var = [1,2,3]
print(var)
var[0] = 2
var[1] = 3
var[2] = 4
print(var)
var.append(6)
print(var)
"""
"""
var = [1,2,3,4,5,6,7]
print(var[:4])
print(var[2:5])
print(var[1:])
"""
"""
var = ["Bob Dylan", "Like a", "Rolling Stone"]
print(var.index("Rolling Stone"))
newlist = var.insert(0, 1985)
print(newlist)
"""
"""
var = ["McCartney", "Lennon", "Starr", "Harrison", "Sutcliffe"]
var.remove("Sutcliffe")
print("After removing Sutcliffe the new list is:", var)
pop1 = var.pop(1)
print("Value popped", pop1)
#pop2 = var.pop(3)
#print(pop2)
print("After popping", pop1, "the new list is:", var)
"""
"""
var = [1,2,3,4,4,6]
print(var)
var[4] = 5
print(var)
var.append(7)
print(var)
print(var[:4])
print(var[2:5])
print(var[5:])
print(var.index(7))
var.insert(0,"0")
print(var)
var.remove(3)
print(var)
print(var.pop(6))
print(var)
"""
"""
tup = (1.25, 9, True, "Hello")
print(tup)
print(tup[1])
print(tup[0])
print(tup[:3])
print(tup[1:])
print(tup[1:3])
"""
"""
tup = ("Bohr", "Leibniz", "Einstein")
empty = []
list1 = [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
for val in tup :
    print(val)
listslice = list1[2:8]
print(listslice)
for ele in listslice:
    empty.append(ele)
print(empty)
"""
"""
tup = (1,2,3,4)
var = tup[1]
print(var)
print(tup[:2])
print(tup[1:3])
print(tup[2:])
for val in tup:
    print(val)
"""
"""
dict = {1:"Hello", 2:"World", 3:"Name"}
print(dict[2])
dict[4] = "Ananth"
print(dict)
"""
"""
dict = {1:"Hello", 2:"World"}
print(dict)
dict[1] = "Vishnu"
print(dict)
del dict[2]
print(dict)
"""
